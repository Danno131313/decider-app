import React from 'react';

class Results extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let factors = this.props.results.factors.map(factor => {
            let items = factor.data.map(item => {
                return <li key={item.name}>{item.name}: <b>{item.value}</b></li>;
            })

            return (
                <li key={factor.name}>
                    <b>{factor.name}</b> stats:
                    <ul>
                        {items}
                    </ul>
                </li>
            );
        });

        let overall = this.props.results.overall.map(item => {
            return <li key={item.name}><b>{item.name}</b> overall adjusted score: <b>{item.score}</b></li>
        })

        return (
            <div className='results'>
                <h2>Results:</h2>
                <ul>
                    {overall}
                </ul>
                <ul>
                    {factors}
                </ul>
            </div>
        );
    }
}

export { Results };