import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './normalize.css';
import { Factors } from './factors.js';
import { Items } from './items';
import { Results } from './results';
import { toContainHTML } from '@testing-library/jest-dom/dist/matchers';

function defaultValue(type) {
  return type === "number" ? 0 : false;
}

function roundDecimal(num) {
  return Math.round(100 * num) / 100;
}

function calcWeight(index) {
  const val = 1 - (0.1 * index);
  return roundDecimal(val);
}

class UI extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      factors: [],
      items: []
    };

    this.addFactor = this.addFactor.bind(this);
    this.reorderFactors = this.reorderFactors.bind(this);
    this.addItem = this.addItem.bind(this);
    this.handleItemChange = this.handleItemChange.bind(this);
    this.handleWeightChange = this.handleWeightChange.bind(this);
    this.deleteFactor = this.deleteFactor.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
    this.generate = this.generate.bind(this);
  }

  addFactor(factor) {
    if (factor.name.trim() === "") {
      return;
    }
    let factors = [...this.state.factors];
    factors.push(factor)
    this.reorderFactors(factors);
  }

  reorderFactors(factors) {
    const items = this.state.items.map(item => {
      let newFactors = [];
      factors.forEach(factor => {
        const prevFactor = item.factors.find(f => f.name === factor.name);
        if (!prevFactor) {
          newFactors.push({ name: factor.name, measurement: factor.measurement, value: defaultValue(factor.measurement) });
        } else {
          newFactors.push({ name: factor.name, measurement: factor.measurement, value: prevFactor.value });
        }
      });
      return { ...item, factors: newFactors };
    });

    factors = factors.map((factor, i) => {
      const oldFactorIndex = this.state.factors.findIndex(f => f.name === factor.name);
      if (oldFactorIndex !== i) {
        factor.weight = calcWeight(i);
      }
      return factor;
    });

    this.setState({
      factors: factors,
      items: items
    });
  }

  addItem(name) {
    let items = [...this.state.items];
    let factors = [...this.state.factors];
    let newItem = { name: name };
    newItem.factors = [];
    factors.forEach((f) => {
      newItem.factors.push({ name: f.name, measurement: f.measurement, value: defaultValue(f.measurement) });
    });
    items.push(newItem);
    this.setState({ items: items });
  }

  handleItemChange(event) {
    const target = event.target;
    let value = target.value;
    const itemFactorNameCombo = target.name;
    const [itemName, factorName] = itemFactorNameCombo.split(":");

    if (this.state.factors.find(f => f.name === factorName).measurement === "number") {
      value = parseInt(value);
      if (isNaN(value)) {
        return;
      }
    } else {
      value = target.checked;
    }

    let state = JSON.parse(JSON.stringify(this.state));

    let itemIndex = state.items.findIndex(i => i.name === itemName);
    let factorIndex = state.items[itemIndex].factors.findIndex(f => f.name === factorName);

    state.items[itemIndex].factors[factorIndex].value = value;
    this.setState(state);
  }

  handleWeightChange(factorName, weight) {
    let factors = [...this.state.factors]
    let factorIndex = factors.findIndex(f => f.name === factorName);
    factors[factorIndex].weight = roundDecimal(weight);
    factors.sort((a, b) => (a.weight < b.weight) ? 1 : -1);
    this.setState({ factors: factors });
  }

  deleteFactor(name) {
    let factors = [...this.state.factors];
    let index = factors.map(f => f.name).indexOf(name);
    factors.splice(index, 1);
    this.reorderFactors(factors);
  }

  deleteItem(name) {
    let items = [...this.state.items];
    let index = items.map(i => i.name).indexOf(name);
    items.splice(index, 1);
    this.setState({
      items: items
    });
  }

  displayError(message) {
    console.log(message);
  }

  generate() {
    let factorData = this.state.factors.map((factor, factorIndex) => {
      let max = 1;
      let min = 0;
      if (factor.measurement === "number") {
        const list = this.state.items.map(i => {
          return i.factors.find(f => f.name === factor.name).value;
        })
        max = Math.max(...list);
        min = Math.min(...list);
      }

      let optimization = factor.optimization;

      if (factor.measurement === "boolean") {
        optimization = optimization ? "maximum" : "minimum";
      }

      return {
        name: factor.name,
        max: max,
        min: min,
        optimization: optimization,
        weight: factor.weight,
        data: this.getItemDataForFactor(factor, max, min, optimization)
      }
    });

    let overall = this.state.items.map(item => {
      let totalScore = 0;
      factorData.forEach(factor => {
        const foundItem = factor.data.find(i => i.name === item.name);
        let val = foundItem.adjustedValue
        console.log(`Factor: ${factor.name}\nFound item adjusted: ${val}`);
        if (factor.optimization === "minimum") {
          val = 1 - val;
        }
        totalScore += val * factor.weight;
        console.log(`Score so far: ${totalScore}`);
      });
      return { name: item.name, score: roundDecimal(totalScore) };
    });

    overall.sort((a, b) => (a.score < b.score) ? 1 : -1);

    this.setState({
      results: {
        factors: factorData,
        overall: overall
      }
    });
  }

  getItemDataForFactor(factor, max, min, optimization) {
    let total = max - min;
    const offset = min;
    let items = this.state.items.map(item => {
      let factorValue = item.factors.find(f => f.name === factor.name).value;
      if (typeof factorValue === "boolean") {
        factorValue = factorValue ? 1 : 0;
      }

      const adjustedValue = (factorValue - offset) / total;
      return { name: item.name, value: factorValue, adjustedValue: adjustedValue };
    });

    if (optimization === "maximum") {
      items.sort((a, b) => (a.value < b.value) ? 1 : -1);
    } else if (optimization === "minimum") {
      items.sort((a, b) => (a.value > b.value) ? 1 : -1);
    }


    return items;
  }

  render() {
    return (
      <div className="ui-main">
        <div className='page-one'>
          <Factors factors={this.state.factors} addFactor={this.addFactor} reorderFactors={this.reorderFactors} deleteFactor={this.deleteFactor} displayError={this.displayError} handleWeightChange={this.handleWeightChange} />
          <Items items={this.state.items} addItem={this.addItem} handleItemChange={this.handleItemChange} deleteItem={this.deleteItem} displayError={this.displayError} />
        </div>
        <button className='generate-button' onClick={this.generate}>Generate</button>
        {this.state.results ? <Results results={this.state.results} /> : ""}
      </div>
    );
  }
}

// ========================================

ReactDOM.render(
  <UI />,
  document.getElementById('root')
);
