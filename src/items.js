import React from 'react';

class Item extends React.Component {
    render() {
        const itemProps = this.props.factors.map((factor, i) => {
            if (factor.measurement === "number") {
                return (
                    <li key={factor.name} className="item-prop">
                        {factor.name}: <input name={this.props.name + ":" + factor.name} value={this.props.factors[i].value} onChange={this.props.handleItemChange} type="number" />
                    </li>
                );
            } else {
                return (
                    <li key={factor.name} className="item-prop">
                        {factor.name}: <input name={this.props.name + ":" + factor.name} checked={this.props.factors[i].value} onChange={this.props.handleItemChange} type="checkbox" />
                    </li>
                );
            }

        });
        return (
            <li key={this.props.name} className="list-item">
                <p><b>{this.props.name}</b></p>
                <ul className='item-props'>
                    {itemProps}
                </ul>
                <button onClick={(e) => this.props.deleteItem(this.props.name)} className="item-delete-button">Delete</button>
            </li>
        );
    }
}

class Items extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            newItem: {
                name: ""
            }
        }

        this.handleChange = this.handleChange.bind(this);
        this.addItem = this.addItem.bind(this);
    }

    handleChange(event) {
        const name = event.target.value;
        this.setState({
            newItem: {
                name: name
            }
        });
    }

    addItem(event) {
        event.preventDefault();

        const name = this.state.newItem.name;

        if (!name || name.trim() === "") {
            this.props.displayError("Name must not be blank");
            return;
        }

        let match = this.props.items.filter((i) => {
            return i.name === name;
        }).length > 0;

        if (match) {
            this.props.displayError("Name must be unique");
            return;
        }

        this.props.addItem(name);

        this.setState({
            newItem: {
                name: ""
            }
        });
    }

    render() {
        const items = this.props.items.map((item, i) => {
            return <Item key={item.name} factors={item.factors} name={item.name} handleItemChange={this.props.handleItemChange} deleteItem={this.props.deleteItem} />;
        });
        return (
            <div className='items'>
                <form id="item-form" onSubmit={this.addItem}>
                    <h2 className='item-form-title'>Add a New Item:</h2>
                    <p className='item-form-desc'>These are the actual things you will be deciding between.</p>
                    <label className='item-name'>
                        Name:
                        <input name="name" type="text" value={this.state.newItem.name} onChange={this.handleChange} />
                    </label>
                    <input className='item-form-submit' type="submit" value="Add" />
                </form>
                <h3>Items:</h3>
                <ul className='item-list'>
                    {items}
                </ul>
            </div>
        );
    }
}

export { Items };