import React from 'react';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: "none",

  // change background colour if dragging
  background: isDragging ? "lightgreen" : "white",

  // styles we need to apply on draggables
  ...draggableStyle
});

const getListStyle = isDraggingOver => ({
  background: isDraggingOver ? "lightblue" : "lightgrey",
  padding: "15px",
});

class Factor extends React.Component {
  render() {
    let text;
    if (this.props.values.optimization === "maximum" || this.props.values.optimization === "minimum") {
      text = <p>Optimized for <b>{this.props.values.optimization}</b> value</p>
    } else if (this.props.values.optimization === "value") {
      text = <p>Optimized for proximity to value <b>{this.props.values.numberOptimizationValue}</b></p>
    } else {
      text = <p>Preference is <b>{this.props.values.optimization ? "True" : "False"}</b></p>
    }

    return (
      <div className='factor'>
        <div className='factor-details'>
          <p><b>{this.props.values.name}</b>: Measured by {this.props.values.measurement}</p>
          {text}
          <label className='factor-weight'>
            Weight:
            <input name="factorWeight" type="number" step="0.01" value={this.props.values.weight} onChange={(e) => this.props.handleWeightChange(this.props.values.name, e.target.value)} />
          </label>
        </div>
        <button onClick={(e) => this.props.deleteFactor(this.props.values.name)} className="factor-delete-button">Delete</button>
      </div>
    );
  }
}

class Factors extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newFactor: {
        name: "",
        measurement: "number",
        optimization: "maximum",
        numberOptimizationValue: 0
      }
    };

    this.handleChange = this.handleChange.bind(this);
    this.addFactor = this.addFactor.bind(this);
    this.onDragEnd = this.onDragEnd.bind(this);
  }

  addFactor(event) {
    event.preventDefault();

    let factor = { ...this.state.newFactor };
    if (factor.name && factor.name.trim() === "") {
      this.props.displayError("Name must not be blank");
      return;
    }

    let match = this.props.factors.filter((f) => {
      return f.name === factor.name;
    }).length > 0;

    if (match) {
      this.props.displayError("Name must be unique");
      return;
    }

    if (factor.measurement === "number" && factor.optimization === "value" && isNaN(parseInt(factor.numberOptimizationValue))) {
      this.props.displayError("Number value must be a valid number!");
      return;
    }

    if (factor.measurement === "boolean") {
      if (factor.optimization !== "true" && factor.optimization !== "false") {
        factor.optimization = true
      } else {
        factor.optimization = factor.optimization === "true";
      }
    }

    this.props.addFactor(factor);

    this.setState({
      newFactor: {
        name: "",
        measurement: "number",
        optimization: "maximum",
        numberOptimizationValue: 0
      }
    });
  }

  displayError() { /* TODO */ }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    let name = target.name;
    if (name === "number-optimize" || name === "boolean-optimize") {
      name = "optimization";
    }
    let state = { ...this.state };
    state.newFactor[name] = value;
    this.setState(state);
  }

  onDragEnd(result) {
    if (!result.destination) {
      return;
    }

    const factors = reorder(
      this.props.factors,
      result.source.index,
      result.destination.index
    );

    this.props.reorderFactors(factors);
  }

  render() {
    return (
      <div className='factors-ui'>
        <form id="factor-form" onSubmit={this.addFactor}>
          <h2 className='factor-form-title'>Add a New Deciding Factor:</h2>
          <p className='factor-form-desc'>
            Price, distance, level of annoyance, whether or not you have to leave the house, etc. <br />
            The weight of each factor describes it's proportion of importance relative to 1.
          </p>
          <label className='factor-name'>
            Name:
            <input name="name" type="text" value={this.state.newFactor.name} onChange={this.handleChange} />
          </label>
          <label className='factor-measurement'>
            Measurement:
            <select name="measurement" value={this.state.newFactor.measurement} onChange={this.handleChange}>
              <option value="number">Number</option>
              <option value="boolean">Boolean</option>
            </select>
          </label>
          <label className={
            this.state.newFactor.measurement === "number" ? "factor-number-optimize" : "factor-number-optimize hidden"
          }>
            Optimize for:
            <select name="number-optimize" value={this.state.newFactor.optimization} onChange={this.handleChange}>
              <option value="maximum">Maximum</option>
              <option value="minimum">Minimum</option>
              <option value="value">Value</option>
            </select>
          </label>
          <label className={
            this.state.newFactor.measurement === "boolean" ? "factor-boolean-optimize" : "factor-boolean-optimize hidden"
          }>
            Preference:
            <select name="boolean-optimize" value={this.state.newFactor.optimization} onChange={this.handleChange}>
              <option value="true">True</option>
              <option value="false">False</option>
            </select>
          </label>
          <label className={
            this.state.newFactor.measurement === "number" && this.state.newFactor.optimization === "value" ? "factor-number-optimize-value" : "factor-number-optimize-value invisible"
          }>
            Value:
            <input name="numberOptimizationValue" type="number" value={this.state.newFactor.numberOptimizationValue} onChange={this.handleChange} />
          </label>
          <input className='factor-form-submit' type="submit" value="Add" />
        </form>
        <h3 className='factor-list-title'>Deciding Factors (by importance):</h3>
        <DragDropContext onDragEnd={this.onDragEnd}>
          <div className='factors-wrapper'>
            <Droppable droppableId="droppable">
              {(provided, snapshot) => (
                <div
                  {...provided.droppableProps}
                  ref={provided.innerRef}
                  style={getListStyle(snapshot.isDraggingOver)}
                >
                  {this.props.factors.map((factor, index) => (
                    <Draggable key={factor.name} draggableId={factor.name} index={index}>
                      {(provided, snapshot) => (
                        <div
                          className='factor-wrapper'
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                          style={getItemStyle(
                            snapshot.isDragging,
                            provided.draggableProps.style
                          )}
                        >
                          <span className='factor-span'>{index + 1}: </span><Factor values={factor} deleteFactor={this.props.deleteFactor} handleWeightChange={this.props.handleWeightChange} />
                        </div>
                      )}
                    </Draggable>
                  ))}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </div>
        </DragDropContext>
      </div >
    );
  }
}

export { Factors };